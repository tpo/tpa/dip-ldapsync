#!/usr/bin/ruby

require 'pp'
require 'net/ldap'
require 'gitlab'
require 'securerandom'
require 'logger'
require 'base64'
require 'digest'
require 'time'
require 'parallel'
require 'toml-rb'
require 'set'

require_relative 'salsa_ldapsync/gitlab_group'
require_relative 'salsa_ldapsync/gitlab_user'
require_relative 'salsa_ldapsync/ldap_user'

$log = Logger.new(STDOUT)

def validate_config(config)
  needed_keys = {
      'ldap' => ['host', 'binddn', 'bindpassword'],
      'gitlab' => ['endpoint', 'token']
  }

  needed_keys.each do |section, keys|
    for key in keys
      unless config.dig(section, key)
        $log.fatal("Could not find required config option '#{ key }' in section '#{ section }'")
        exit(1)
      end
    end
  end
end

def load_config()
  filename = 'ldapsync.toml'
  config_files = Array.new
  config_files.push("#{ENV['HOME']}/.config/#{ filename }")
  config_files.push(File.join(__dir__, filename))
  config_file = nil
  config_files.each{ |file|
    if File::file?(file) and File::readable?(file)
      config_file = file
      break
    end
  }
  unless config_file
    $log.fatal("Could not load configuration file, tried #{config_files.inspect}")
    exit(1)
  end
  $log.debug("Load configuration file #{ config_file }")
  config = TomlRB.load_file(config_file)
  validate_config(config)
  config
end

def create_user (ldap_user, source)
  gitlab_user = $gitlab_users.create(username: ldap_user.username, name: ldap_user.name)
  gitlab_user.custom_attributes['source'] = source
  update_user(ldap_user, gitlab_user)
end

def update_user (ldap_user, gitlab_user)
  if ldap_user.state == 'active'
    unblock_user(ldap_user, gitlab_user)
  else
    block_user(gitlab_user)
  end
end

def unblock_user (ldap_user, gitlab_user)
  gitlab_user.state = 'active'

  if $gitlab_group_members&.has_key?(gitlab_user.username) == false
    $gitlab_group_members.add(gitlab_user)
  end

  #gitlab_user.sync_sshkeys!(ldap_user.sshkeys)
  gitlab_user.sshkeys = ldap_user.sshkeys
end

def block_user (gitlab_user)
  gitlab_user.state = 'blocked'

  if $gitlab_group_members&.has_key?(gitlab_user.username) == true
    $gitlab_group_members.delete(gitlab_user)
  end
end

if ENV['LOGLEVEL']
  $log.level = Logger.const_get(ENV['LOGLEVEL'])
else
  $log.level = Logger::WARN
end

config = load_config

$log.level = Logger.const_get(config.dig('log', 'level')) if config.dig('log', 'level')

ldap = Net::LDAP.new(
  :host => config['ldap']['host'],
  :port => config.dig(:ldap, 'port') || 636,
  :encryption => :simple_tls,
  :auth => {
    :method => :simple,
    :username => config['ldap']['binddn'],
    :password => config['ldap']['bindpassword'],
  })

if not ldap.bind
  $log.fatal("could not bind to LDAP Host #{ config['ldap']['host'] }")
  exit (1)
end

Gitlab.configure do |c|
  c.endpoint = config['gitlab']['endpoint'] # API endpoint URL, default: ENV['GITLAB_API_ENDPOINT']
  c.private_token = config['gitlab']['token'] # user's private token or OAuth2 access token, default: ENV['GITLAB_API_PRIVATE_TOKEN']
end
$client = Gitlab.client()

source = 'ldapsync'

begin
  $gitlab_group_members = SalsaLdapsync::GitlabGroupMembers.new $client, 'torproject'
rescue
  $gitlab_group_members = nil
end

$gitlab_users = SalsaLdapsync::GitlabUsers.new $client, custom_attributes: { :source => source }

$ldap_users = SalsaLdapsync::LdapUsers.new ldap, :base => config['ldap']['base'], :filter => config['ldap']['filter']

gitlab_users = $gitlab_users.each_key.to_set
ldap_users = $ldap_users.each_key.to_set

(ldap_users - gitlab_users).each do |uid|
  ldap_user = $ldap_users[uid]
  create_user(ldap_user, source)
end

(ldap_users & gitlab_users).each do |uid|
  ldap_user = $ldap_users[uid]
  gitlab_user = $gitlab_users[uid]
  update_user(ldap_user, gitlab_user)
end

(gitlab_users - ldap_users).each do |uid|
  gitlab_user = $gitlab_users[uid]
  block_user(gitlab_user)
end
