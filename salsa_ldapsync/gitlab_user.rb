require 'gitlab'

class Gitlab::Client
  module Users
    def delete_user_custom_attribute(id, key)
      delete("/users/#{id}/custom_attributes/#{key}")
    end

    def set_user_custom_attribute(id, key, value)
      put("/users/#{id}/custom_attributes/#{key}", body: { :value => value })
    end
  end
end

module SalsaLdapsync
  class GitlabUser
    class CustomAttributes
      extend Forwardable

      def_delegators :@data, :[], :has_key?, :each, :each_key, :each_pair, :each_value

      def initialize(client, id, username, custom_attributes)
        @client = client
        @id = id
        @username = username
        @data = custom_attributes
      end

      def []=(key, value)
        if @data[key] != value
          $log&.info("Setting custom attribute #{key} of user #{@username} to #{value}")
          @client.set_user_custom_attribute(@id, key, value)
          @data[key] = value
        end
        @data[key]
      end

      def delete(key)
        if has_key?(key)
          $log&.info("Deleting custom attribute #{key} of user #{@username}")
          @client.delete_user_custom_attribute(@id, key)
          @data.delete(key)
        elsif block_given?
          yield key
        end
      end
    end

    SYNCED_SSHKEY_PREFIX = 'synced-sshkey-'

    attr_reader :id, :username, :state, :custom_attributes

    def initialize(client, info)
      @client = client
      @id = info.id
      @username = info.username
      @state = info.state
      @custom_attributes = CustomAttributes.new @client, @id, @username, \
        info.to_h.fetch('custom_attributes', {}).each_with_object({}) { |t, h| h[t['key']] = t['value'] }
    end

    def state=(new_state)
      if @state != new_state
        if new_state == 'active'
          $log&.info("Unblocking user #{@username}")
          @client.unblock_user(@id)
        elsif new_state == 'blocked'
          $log&.info("Blocking user #{@username}")
          @client.block_user(@id)
        end
        @state = new_state
      end
      @state
    end

    def sshkeys_id
      custom_attributes.each_pair.map do |key, value|
        if key.start_with?(SYNCED_SSHKEY_PREFIX)
          i = key.split('-')
          id = i[-1].to_i
          [SshKey.from_string(value).freeze, id]
        end
      end.compact.to_h
    end

    def create_sshkey(key)
      $log&.info("Add SSH key '#{key.comment}' to user #{@username}")
      date = Time.now.strftime("%Y-%m-%d")
      title = "#{key.comment} (synced from LDAP on #{date})"

      gitlab_key = @client.create_ssh_key(title, key.to_s_minimal, {user_id: id})
      custom_attributes[SYNCED_SSHKEY_PREFIX + gitlab_key.id.to_s] = key.to_s_minimal
    end

    def delete_sshkey(key)
      key_id = sshkeys_id.fetch(key)

      $log&.info("Deleting SSH key #{key_id} from user #{@username}")

      custom_attributes.delete(SYNCED_SSHKEY_PREFIX + key_id.to_s)
      @client.delete_ssh_key(key_id, {user_id: id})
    end

    def sshkeys=(new_keys)
      old_keys_id = sshkeys_id
      old_keys = old_keys_id.each_key.to_set

      add_keys = new_keys - old_keys
      delete_keys = old_keys - new_keys

      add_keys.each do |key|
        begin
          create_sshkey key
        rescue Exception => e
          $log&.warn("Could not add key '#{key.comment}' to #{username}: #{e.message}")
        end
      end

      delete_keys.each do |key|
        begin
          delete_sshkey key
        rescue Exception => e
          $log&.warn("Could not remove key #{key_id} from #{username}: #{e.message}")
        end
      end
    end

    def sync_sshkeys!(new_keys)
      old_keys_id = @client.ssh_keys({user_id: @id}).map do |key|
        [SshKey.from_string(key.key).freeze, key.id]
      end.to_h
      old_keys = old_keys_id.each_key.to_set

      sync_keys = old_keys & new_keys

      sync_keys.each do |key|
        id = old_keys_id.fetch(key)
        $log.info("Sync SSH key '#{key.comment}' for user #{@username}")
        custom_attributes[SYNCED_SSHKEY_PREFIX + id.to_s] = key.to_s_minimal
      end
    end
  end

  class GitlabUsers
    extend Forwardable

    PAGE_COUNT = 100  # XXX: 10000 users should be enough
    PAGE_SIZE = 100

    def_delegators :@users, :[], :has_key?, :each, :each_key, :each_value

    def initialize(client, custom_attributes: {})
      @client = client

      @users = {}

      (1..PAGE_COUNT).each do |page|
        users = client.users({
          :per_page => PAGE_SIZE,
          :page => page,
          :order_by => 'id',
          :sort => 'asc',
          :custom_attributes => custom_attributes,
          :with_custom_attributes => 'true',
        })
        if users.length == 0
          break
        end
        $log.debug("Read Gitlab users page #{page}")
        users.each do |user|
          @users[user.username] = GitlabUser.new @client, user
        end
      end
    end

    def create(username:, name:)
      $log.info("Creating user #{username}")

      begin
        user = $client.create_user(
          "#{username}@torproject.org",
          SecureRandom.hex,
          username,
          {
            name: name,
            can_create_group: false,
            skip_confirmation: true
          }
        )

      rescue Gitlab::Error::Conflict => e
        begin
          user = $client.users({ :username => username })[0]
        rescue Exception => e2
          $log.error("Could not get #{username} after conflict")
          $log.error(e2.message)
          $log.error(e.message)
        end

      rescue Exception => e
        $log.error("Could not create #{username}")
        $log.error(e.message)
      end

      @users[username] = GitlabUser.new @client, user if user
    end
  end
end
