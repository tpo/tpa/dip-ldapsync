module SalsaLdapsync
  class SshKey
    VALID_TYPES = Set['ssh-rsa', 'ssh-ed25519']
    RE = Regexp.new %q{
      ^
      (
        allowed_hosts=
        (?<allowed_hosts>[^\s]*)
        \s
      )?
      (
        (?<options>.*?)
        \s
      )?
      (?<type>ssh-(rsa|ed25519))
      \s
      (?<key>[a-zA-Z0-9=/+]+)
      (
        \s
        (?<comment>.*)
      )?
      $
    }, Regexp::EXTENDED

    attr_reader :type, :key, :comment, :options

    def initialize(type:, key:, comment: nil, options: nil)
      if not VALID_TYPES.include?(type)
        raise ArgumentError, "Unsupported key type #{type}"
      end

      @type = type
      @key = key
      @comment = comment
      @options = options
    end

    def self.from_string(s)
      m = RE.match(s)
      if not m
        raise ArgumentError, "Unsupported key in #{s}"
      end

      lo = []

      # Ignore Debian LDAP allowed_hosts extension
      if m[:allowed_hosts]
        lo << 'allowed_hosts'
      end

      if m[:options]
        lo << m[:options]
      end

      type = m[:type]
      key = m[:key]
      comment = m[:comment]
      if lo.length > 0
        options = lo.join(',')
      end

      SshKey.new(type: type, key: key, comment: comment, options: options)
    end

    def eql?(other)
      type.eql?(other.type) && key.eql?(other.key)
    end

    def hash
      type.hash ^ key.hash
    end

    def to_s
      a = []
      a << @options if @options
      a << @type
      a << @key
      a << @comment if @comment
      a.join(' ')
    end

    def to_s_minimal
      a = []
      a << @type
      a << @key
      a.join(' ')
    end
  end
end
